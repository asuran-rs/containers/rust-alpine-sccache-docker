FROM rust:alpine

RUN apk add --no-cache \
        alpine-sdk cmake openssl-dev linux-headers lld

RUN rustc --version

ENV CARGO_HOME=/opt/.cargo

RUN mkdir -p /opt/.cargo

RUN rustup component add rustfmt clippy

#ENV OPENSSL_STATIC=yes

#RUN env RUSTFLAGS='-Clink-arg=-Wl,-Bstatic -Clink-arg=-lc' cargo install sccache --features all
RUN cargo install --force cargo-strip

COPY cargo.config /opt/.cargo/config
